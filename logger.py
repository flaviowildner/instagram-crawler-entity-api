import logging


def start_logger():
    # Logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Handler
    c_handler = logging.StreamHandler()
    f_handler = logging.FileHandler('file.log')

    c_handler.setLevel(logging.INFO)
    f_handler.setLevel(logging.WARN)

    c_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    f_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    c_handler.setFormatter(c_formatter)
    f_handler.setFormatter(f_formatter)

    logger.addHandler(c_handler)
    logger.addHandler(f_handler)
