import logging
from time import sleep

from peewee_migrate import Router

from peewee import PostgresqlDatabase, OperationalError

from constants.consts import DB


def run_migrations():
    psql_db = Router(
        PostgresqlDatabase(DB['database'], user=DB['user'], password=DB['password'], host=DB['host']))

    retries = 0
    while True:
        try:
            psql_db.run()
            break
        except OperationalError as ex:
            logging.exception(ex)
            sleep(min(retries * 5, 60))
            retries += 1
