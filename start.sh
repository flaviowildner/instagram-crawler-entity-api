#!/bin/bash
set -e
python3 -m virtualenv venv
. venv/bin/activate
pip install -r requirements.txt

export FLASK_APP=main.py
flask run