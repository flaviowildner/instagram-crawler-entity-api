import logging
from json.decoder import JSONDecodeError
from typing import List

from flask import request, jsonify
from marshmallow import ValidationError

from model.post import Post
from resources import post_blueprint, post_schema
from resources.dtos.post_dto import PostDTO
from services import post_service, profile_service
from services.post_service import RateConfig

rate_config = RateConfig()


@post_blueprint.route('/get_posts_to_crawl', methods=['GET'])
def get_posts_to_crawl():
    n_posts = request.args.get('n_posts')
    if n_posts is None:
        logging.warning(f'Missing parameters on calling get_posts_to_crawl.')
        return {'message': "n_posts param is empty"}, 400

    try:
        posts: List[Post] = post_service.get_n_to_crawl(int(n_posts), rate_config.rate)
    except RuntimeError as e:
        logging.error(f'Error when getting list of awaiting(to be crawled) posts.')
        return {'message': e.__str__()}, 500

    posts_ids = ["'" + post.id_.__str__() + "'" for post in posts]
    logging.info(f'List of awaiting(to be crawled) posts returned. - Post ids: {{{", ".join(posts_ids)}}}')
    return jsonify([model_to_dto(post).__dict__ for post in posts])


@post_blueprint.route('/', methods=['PUT'])
def save_post():
    try:
        post: PostDTO = post_schema.loads(request.data, unknown='EXCLUDE')
    except ValidationError as e:
        return {'message': e.messages}, 400
    except JSONDecodeError:
        return {'message': 'Invalid JSON'}, 400

    try:
        saved_post = post_service.save(dto_to_model(post))
    except RuntimeError as e:
        logging.info(f'Post saved! - {post.url}')
        return {'message': e.__str__()}, 500

    logging.info(f'Post saved! - {post.url}')
    return model_to_dto(saved_post).__dict__


@post_blueprint.route('/', methods=['GET'])
def get_post():
    url_param = request.args.get('url')
    if url_param is None:
        logging.warning(f'Missing parameters on calling get_post.')
        return {'message': "url param is empty"}, 400

    try:
        post_model = post_service.get(url_param)
        if post_model is None:
            logging.warning(f'The requested post doesn\'t exist! - {url_param}')
            return {'message': 'Not found'}, 404
    except RuntimeError as e:
        logging.error(f'Error when getting post. - {url_param} - {e.__str__()}')
        return None, 500

    logging.info(f'Post returned! - {url_param}')
    return model_to_dto(post_model).__dict__


@post_blueprint.route('/rate', methods=['PUT'])
def set_revisited_visit_rate_variable():
    rate = request.args.get('rate')
    if rate is None:
        logging.warning(f'Missing parameters on calling set_revisited_visit_rate_variable.')
        return {'message': "rate param is empty"}, 400

    global rate_config
    rate_config.rate = float(rate)
    return ''


def dto_to_model(post_dto: PostDTO) -> Post:
    profile = profile_service.get_or_create_profile(post_dto.username)
    return Post(profile=profile, url=post_dto.url, url_imgs=post_dto.url_imgs,
                post_date=post_dto.post_date, caption=post_dto.caption, likes=post_dto.likes)


def model_to_dto(post: Post) -> PostDTO:
    return PostDTO(username=post.profile.username, url=post.url, url_imgs=post.url_imgs, post_date=post.post_date,
                   caption=post.caption, likes=post.likes)
