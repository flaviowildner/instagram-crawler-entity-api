import logging
from json.decoder import JSONDecodeError
from typing import List

from flask import request, jsonify
from marshmallow import ValidationError

from model.profile import Profile
from resources import profile_blueprint, profile_schema
from resources.dtos.profile_dto import ProfileDTO
from services import profile_service
from services.profile_service import RateConfig

rate_config = RateConfig()


@profile_blueprint.route('/', methods=['PUT'])
def create_or_update_profile():
    service_identifier = request.args.get('service_identifier')

    if service_identifier is None:
        logging.warning(f'Missing parameters on calling create_or_update_profile.')
        return {'message': "service_identifier param is empty"}, 400

    try:
        profile: ProfileDTO = profile_schema.loads(request.data, unknown='EXCLUDE')
    except ValidationError as e:
        return {'message': e.messages}, 400
    except JSONDecodeError:
        return {'message': 'Invalid JSON'}, 400

    try:
        saved_profile = profile_service.create_or_update(dto_to_model(profile), service_identifier)
        logging.info(f'Profile created! - @{saved_profile.username}')
        return model_to_dto(saved_profile).__dict__
    except Exception as e:
        logging.error(f'Error when saving profile. - @{profile.username}')
        return {'message': e.__str__()}, 500


@profile_blueprint.route('/batch', methods=['PUT'])
def create_or_update_profile_batch():
    service_identifier = request.args.get('service_identifier')

    if service_identifier is None:
        logging.warning(f'Missing parameters on calling create_or_update_profile_batch.')
        return {'message': "service_identifier param is empty"}, 400

    try:
        profiles: List[ProfileDTO] = profile_schema.loads(request.data, many=True, unknown='EXCLUDE')
    except ValidationError as e:
        return {'message': e.messages}, 400
    except JSONDecodeError:
        return {'message': 'Invalid JSON'}, 400

    saved_profiles = []
    errors = []
    for profile in profiles:
        try:
            saved_profile = profile_service.create_or_update(dto_to_model(profile), service_identifier)
            saved_profiles.append(saved_profile)
            logging.info(f'Profile created! - @{saved_profile.username}')
        except Exception as e:
            logging.error(f'Error when saving profile. - @{profile.username} - {e.__str__()}')
            errors.append(f"Error when saving profile '{profile.username}'")

    return {'data': [model_to_dto(saved_profile).__dict__ for saved_profile in saved_profiles], 'errors': errors}


@profile_blueprint.route('/<username>', methods=['GET'])
def get_or_create_profile(username: str):
    try:
        profile: Profile = profile_service.get_or_create(username)
        logging.info(f'Profile returned! - @{username}')
        return model_to_dto(profile).__dict__
    except Exception as e:
        logging.error(f'Error when getting profile. - @{username} - {e.__str__()}')
        return None, 500


@profile_blueprint.route('/rate', methods=['PUT'])
def set_revisited_visit_rate_variable():
    rate = request.args.get('rate')
    if rate is None:
        logging.warning(f'Missing parameters on calling set_revisited_visit_rate_variable.')
        return {'message': "rate param is empty"}, 400

    global rate_config
    rate_config.rate = float(rate)
    return ''


def dto_to_model(profile_dto: ProfileDTO):
    return Profile(username=profile_dto.username, name=profile_dto.name,
                   description=profile_dto.description,
                   n_followers=profile_dto.n_followers, n_following=profile_dto.n_following,
                   n_posts=profile_dto.n_posts, photo_url=profile_dto.photo_url, followers=[],
                   followings=[])


def model_to_dto(profile: Profile):
    return ProfileDTO(username=profile.username, name=profile.name, description=profile.description,
                      n_followers=profile.n_followers, n_following=profile.n_following, n_posts=profile.n_posts,
                      photo_url=profile.photo_url)
