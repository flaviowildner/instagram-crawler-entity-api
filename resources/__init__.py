from flask import Blueprint

from resources.schemas.post_media_schema import PostMediaDTOSchema
from resources.schemas.post_schema import PostDTOSchema
from resources.schemas.profile_schema import ProfileDTOSchema

# profile
profile_blueprint = Blueprint('profile', __name__, url_prefix='/profile')
profile_schema = ProfileDTOSchema()

# profile control
profile_control_blueprint = Blueprint('profile_control', __name__, url_prefix='/profile_control')

# priority profile
priority_profile_blueprint = Blueprint('priority_profile', __name__, url_prefix='/priority_profile')

# post
post_blueprint = Blueprint('post', __name__, url_prefix='/post')
post_schema = PostDTOSchema()

# profile control
post_control_blueprint = Blueprint('post_control', __name__, url_prefix='/post_control')

# post media
post_media_blueprint = Blueprint('post_media', __name__, url_prefix='/post_media')
post_media_schema = PostMediaDTOSchema()
