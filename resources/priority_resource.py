import logging

from flask import request

from resources import priority_profile_blueprint
from services import priority_service


@priority_profile_blueprint.route('/', methods=['PUT'])
def add_priority_profile():
    errors = []

    username = request.args.get('username')
    service_identifier = request.args.get('service_identifier')

    if username is None:
        logging.warning(f'Missing parameters on calling add_priority_profile.')
        errors.append("username param is empty")

    if service_identifier is None:
        logging.warning(f'Missing parameters on calling add_priority_profile.')
        errors.append("service_identifier param is empty")

    if len(errors) > 0:
        return {'errors': ', '.join(errors)}, 400

    priority_service.add_priority_profile(username, service_identifier)

    return "", 200
