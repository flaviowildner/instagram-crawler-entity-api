import logging
from json.decoder import JSONDecodeError

from flask import request
from marshmallow import ValidationError

from model.post import Post
from model.post_media import PostMedia
from resources import post_media_blueprint, post_media_schema
from resources.dtos.post_media_dto import PostMediaDTO
from services import post_media_service
from services import post_service


@post_media_blueprint.route('/', methods=['POST'])
def save_post_media():
    try:
        post_media: PostMediaDTO = post_media_schema.loads(request.data, unknown='EXCLUDE')
    except ValidationError as e:
        return {'message': e.messages}, 400
    except JSONDecodeError:
        return {'message': 'Invalid JSON'}, 400

    try:
        post_media_service.save(dto_to_model(post_media))
    except RuntimeError as e:
        logging.error(f'Error when saving post_media. - {post_media.media_url} - {e.__str__()}')
        return {'message': e.__str__()}, 500

    logging.info(f'Post media saved! - {post_media.media_url}')
    return 'success', 201


def dto_to_model(post_media: PostMediaDTO) -> PostMedia:
    post: Post = post_service.get(post_media.post_url)
    if post is None:
        raise RuntimeError('Post doesn\'t exist')

    return PostMedia(post=post, media_url=post_media.media_url, extension=post_media.extension, data=post_media.data,
                     post_media_date=post_media.post_media_date)
