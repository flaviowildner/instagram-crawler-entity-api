class ProfilePriorityDTO:
    username: str
    service_identifier: str
    created_at: int

    def __init__(self,
                 username: str = None,
                 service_identifier: str = None,
                 created_at: int = None,
                 **kwargs) -> None:
        self.username = username
        self.service_identifier = service_identifier
        self.created_at = created_at
