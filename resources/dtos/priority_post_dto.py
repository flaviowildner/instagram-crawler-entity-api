class PostPriorityDTO:
    url: str
    service_identifier: str
    created_at: int

    def __init__(self,
                 url: str = None,
                 service_identifier: str = None,
                 created_at: int = None,
                 **kwargs) -> None:
        self.url = url
        self.service_identifier = service_identifier
        self.created_at = created_at
