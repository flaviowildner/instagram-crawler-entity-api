class ProfileWatchlistDTO:
    username: str
    service_identifier: str
    expiration_at: int
    created_at: int

    def __init__(self, username: str = None,
                 service_identifier: str = None,
                 expiration_at: int = None,
                 created_at: int = None) -> None:
        self.username = username
        self.service_identifier = service_identifier
        self.expiration_at = expiration_at
        self.created_at = created_at
