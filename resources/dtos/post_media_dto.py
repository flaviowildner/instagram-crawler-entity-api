class PostMediaDTO:
    post_url: str
    media_url: str
    extension: str
    data: bytes
    post_media_date: int

    def __init__(self, post_url=None,
                 media_url=None,
                 extension=None,
                 data=None,
                 post_media_date=None,
                 **kwargs) -> None:
        self.post_url = post_url
        self.media_url = media_url
        self.extension = extension
        self.data = data
        self.post_media_date = post_media_date
