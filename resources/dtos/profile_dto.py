class ProfileDTO:
    username: str
    name: str
    description: str
    n_followers: int
    n_following: int
    n_posts: int
    photo_url: str

    def __init__(self, username=None,
                 name=None,
                 description=None,
                 n_followers=None,
                 n_following=None,
                 n_posts=None,
                 photo_url=None,
                 **kwargs) -> None:
        self.username = username
        self.name = name
        self.description = description
        self.n_followers = n_followers
        self.n_following = n_following
        self.n_posts = n_posts
        self.photo_url = photo_url
