import json
import logging
from json import JSONDecodeError
from typing import List

from flask import request, jsonify
from marshmallow import ValidationError

from resources import profile_control_blueprint
from resources.profile_resource import rate_config
from services import profile_control_service


@profile_control_blueprint.route('/', methods=['POST'])
def create_profile_control():
    username = request.args.get('username')
    service_identifier = request.args.get('service_identifier')

    errors = []

    if username is None:
        logging.warning(f'Missing parameters on calling create_profile_control.')
        errors.append("username param is empty")

    if service_identifier is None:
        logging.warning(f'Missing parameters on calling create_profile_control.')
        errors.append("service_identifier param is empty")

    if len(errors) > 0:
        return {'errors': ', '.join(errors)}, 400

    try:
        profile_control_service.create_profile_control(username)
    except Exception as e:
        logging.error(f'Error when saving profile control. - @{username} - {e.__str__()}')
        errors.append(f'Error when saving profile control. - @{username} - {e.__str__()}')

    return "", 200


@profile_control_blueprint.route('/batch', methods=['POST'])
def create_profile_control_batch():
    errors = []

    try:
        profile_controls_usernames: List[str] = json.loads(request.data)
    except ValidationError as e:
        for error_message in e.messages:
            errors.append(error_message)

        return errors
    except JSONDecodeError:
        errors.append('Invalid JSON')
        return errors

    saved_profile_control_usernames = []

    for profile_control_username in profile_controls_usernames:
        try:
            profile_control_service.create_profile_control(profile_control_username)
            saved_profile_control_usernames.append(profile_control_username)
            logging.info(f'Profile control created! - @{profile_control_username}')
        except Exception as e:
            logging.error(f'Error when saving profile control. - @{profile_control_username} - {e.__str__()}')
            errors.append(f'Error when saving profile control. - @{profile_control_username} - {e.__str__()}')

    return {'data': [saved_profile_username for saved_profile_username in saved_profile_control_usernames],
            'errors': errors}


@profile_control_blueprint.route('/get_profiles_to_crawl', methods=['GET'])
def get_profiles_to_crawl():
    n_profiles = request.args.get('n_profiles')
    service_identifier = request.args.get('service_identifier')

    errors = []

    if n_profiles is None:
        logging.warning(f'Missing parameters on calling get_profiles_to_crawl.')
        errors.append("n_profiles param is empty")

    if service_identifier is None:
        logging.warning(f'Missing parameters on calling get_profiles_to_crawl.')
        errors.append("service_identifier param is empty")

    if len(errors) > 0:
        return {'errors': ', '.join(errors)}, 400

    try:
        username_list: List[str] = profile_control_service.get_n_to_crawl(int(n_profiles),
                                                                          rate_config.rate,
                                                                          service_identifier)
    except RuntimeError as e:
        logging.error(f'Error when getting list of awaiting(to be crawled) profiles.')
        return {'message': e.__str__()}, 500

    usernames = ["'" + username + "'" for username in username_list]
    logging.info(f'List of awaiting(to be crawled) profiles returned. - {{{", ".join(usernames)}}}')
    return jsonify(username_list)
