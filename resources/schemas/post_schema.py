from marshmallow import fields, post_load

from resources.dtos.post_dto import PostDTO
from resources.schemas.base_schema import BaseSchema


class PostDTOSchema(BaseSchema):
    username = fields.Str(required=True)
    url = fields.Str(required=True)
    url_imgs = fields.List(fields.Str())
    post_date = fields.Integer()
    caption = fields.Str()
    likes = fields.Integer()

    @post_load
    def make_post(self, data, **kwargs):
        return PostDTO(**data)
