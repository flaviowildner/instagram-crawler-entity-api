import base64

from marshmallow import fields, post_load

from resources.dtos.post_media_dto import PostMediaDTO
from resources.schemas.base_schema import BaseSchema


class PostMediaDTOSchema(BaseSchema):
    post_url = fields.Str(required=True)
    media_url = fields.Str(required=True)
    extension = fields.Str()
    data = fields.Str()
    post_media_date = fields.Integer()

    @post_load
    def make_post_media(self, data, **kwargs):
        return PostMediaDTO(post_url=data.get('post_url'), media_url=data.get('media_url'),
                            extension=data.get('extension'),
                            data=base64.b64decode(data['data']),
                            post_media_date=data.get('post_media_date'))
