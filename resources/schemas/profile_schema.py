from marshmallow import fields, post_load

from resources.dtos.profile_dto import ProfileDTO
from resources.schemas.base_schema import BaseSchema


class ProfileDTOSchema(BaseSchema):
    username = fields.Str(required=True)
    name = fields.Str()
    description = fields.Str()
    n_followers = fields.Integer()
    n_following = fields.Integer()
    n_posts = fields.Integer()
    photo_url = fields.Str()

    @post_load
    def make_profile(self, data, **kwargs):
        return ProfileDTO(**data)
