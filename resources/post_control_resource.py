import json
import logging
from json import JSONDecodeError
from typing import List

from flask import request, jsonify
from marshmallow import ValidationError

from resources import post_control_blueprint
from resources.post_resource import rate_config
from services import post_control_service


@post_control_blueprint.route('/', methods=['POST'])
def create_post_control():
    url = request.args.get('url')

    errors = []

    if url is None:
        logging.warning(f'Missing parameters on calling create_post_control.')
        errors.append("url param is empty")

    if len(errors) > 0:
        return {'errors': ', '.join(errors)}, 400

    try:
        post_control_service.create_post_control(url)
    except Exception as e:
        logging.error(f'Error when saving post control. - @{url} - {e.__str__()}')
        errors.append(f'Error when saving post control. - @{url} - {e.__str__()}')

    return "", 200


@post_control_blueprint.route('/batch', methods=['POST'])
def create_post_control_batch():
    errors = []

    try:
        post_controls_urls: List[str] = json.loads(request.data)
    except ValidationError as e:
        for error_message in e.messages:
            errors.append(error_message)

        return errors
    except JSONDecodeError:
        errors.append('Invalid JSON')
        return errors

    saved_post_control_urls = []

    for post_control_url in post_controls_urls:
        try:
            post_control_service.create_post_control(post_control_url)
            saved_post_control_urls.append(post_control_url)
            logging.info(f'Post control created! - @{post_control_url}')
        except Exception as e:
            logging.error(f'Error when saving post control. - @{post_control_url} - {e.__str__()}')
            errors.append(f'Error when saving post control. - @{post_control_url} - {e.__str__()}')

    return {'data': [saved_post_url for saved_post_url in saved_post_control_urls],
            'errors': errors}


@post_control_blueprint.route('/get_posts_to_crawl', methods=['GET'])
def get_posts_to_crawl():
    n_posts = request.args.get('n_posts')
    service_identifier = request.args.get('service_identifier')

    errors = []

    if n_posts is None:
        logging.warning(f'Missing parameters on calling get_posts_to_crawl.')
        errors.append("n_posts param is empty")

    if service_identifier is None:
        logging.warning(f'Missing parameters on calling get_posts_to_crawl.')
        errors.append("service_identifier param is empty")

    if len(errors) > 0:
        return {'errors': ', '.join(errors)}, 400

    try:
        post_url_list: List[str] = post_control_service.get_n_to_crawl(int(n_posts),
                                                                       rate_config.rate,
                                                                       service_identifier)
    except RuntimeError as e:
        logging.error(f'Error when getting list of awaiting(to be crawled) posts.')
        return {'message': e.__str__()}, 500

    post_urls = ["'" + post_url + "'" for post_url in post_url_list]
    logging.info(f'List of awaiting(to be crawled) posts returned. - {{{", ".join(post_urls)}}}')
    return jsonify(post_url_list)
