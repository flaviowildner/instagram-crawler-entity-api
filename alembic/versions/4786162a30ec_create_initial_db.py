"""create initial db

Revision ID: 4786162a30ec
Revises: 
Create Date: 2021-04-07 20:31:03.465888

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4786162a30ec'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # op.create_table(
    #     'testeIvo',
    #     sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
    #     sa.Column('name', sa.String(50), nullable=False),
    #     sa.Column('description', sa.Unicode(200)),
    # )
    op.execute("""
        begin transaction;
            create table profile
                            (
                                id          serial not null
                                    constraint profile_pkey
                                        primary key,
                                username    text   not null,
                                name        text,
                                description text,
                                n_followers integer,
                                n_following integer,
                                n_posts     integer,
                                photo_url   text,
                                last_visit  bigint,
                                created_at  bigint,
                                deleted     boolean,
                                visited     boolean,
                                delay       integer
                            );
        
            create table post
                            (
                                id         serial not null
                                    constraint post_pkey
                                        primary key,
                                profile_id integer
                                    constraint post_profile_id_fkey
                                        references profile,
                                url        text,
                                url_imgs   text,
                                post_date  bigint,
                                caption    text,
                                last_visit bigint,
                                created_at bigint,
                                deleted    boolean,
                                delay      integer,
                                likes      integer
                            );
        
        
            create table post_media
                            (
                                post_id integer not null
                                    constraint post_media_post_id_fkey
                                        references post,
                                media_url text not null,
                                extension text,
                                data bytea,
                                post_media_date bigint,
                                constraint post_media_pkey
                                    primary key (post_id, media_url)
                            );
            
            
        commit
    """)


def downgrade():
    op.drop_table("profile")
    op.drop_table("post")
    op.drop_table("post_media")
