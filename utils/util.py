def merge_dict(source: dict, dest: dict):
    res = dest.copy()

    for key, value in source.items():
        if value is not None:
            res[key] = value

    return res
