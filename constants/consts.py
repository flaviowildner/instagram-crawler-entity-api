import os

DB = {'host': os.environ.get('DB_HOST', ''),
      'user': os.environ.get('DB_USER', ''),
      'password': os.environ.get('DB_PASSWORD', ''),
      'database': os.environ.get('DB_DATABASE', '')}

SERVICE_IDENTIFIERS_FOR_PROFILES = ['description', 'follow', 'post']
SERVICE_IDENTIFIERS_FOR_POSTS_URLS = ['likers', 'post_downloader']
