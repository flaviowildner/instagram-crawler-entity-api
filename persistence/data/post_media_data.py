from datetime import datetime
from typing import Optional

import persistence.data.post_data as post_data
from model.post_media import PostMedia
from persistence.entity.post_media_entity import PostMediaEntity


def save(post_media: PostMedia):
    post_media.post_media_date = int(datetime.now().timestamp())

    post_media_entity = to_entity(post_media)

    try:
        PostMediaEntity.get(PostMediaEntity.post == post_media.post.id_,
                            PostMediaEntity.media_url == post_media.media_url)
        post_media_entity.save()
    except PostMediaEntity.DoesNotExist:
        post_media_entity.save(force_insert=True)


def get_by_post_id(post_id: int):
    return [from_entity(post_media) for post_media in PostMediaEntity.select().where(PostMediaEntity.post == post_id)]


def get_by_url(url: str) -> Optional[PostMedia]:
    try:
        return from_entity(PostMediaEntity.get(PostMediaEntity.media_url == url))
    except PostMediaEntity.DoesNotExist:
        return None


def from_entity(post_media_entity: PostMediaEntity) -> PostMedia:
    return PostMedia(
        post=post_data.from_entity(post_media_entity.post),
        media_url=post_media_entity.media_url,
        extension=post_media_entity.extension,
        data=post_media_entity.data,
        post_media_date=post_media_entity.post_media_date
    )


def to_entity(post_media: PostMedia) -> PostMediaEntity:
    return PostMediaEntity(
        post=post_data.to_entity(post_media.post),
        media_url=post_media.media_url,
        extension=post_media.extension,
        data=post_media.data,
        post_media_date=post_media.post_media_date
    )
