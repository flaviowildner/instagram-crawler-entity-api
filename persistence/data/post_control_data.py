from datetime import datetime
from typing import List

from model.post_priority import PostPriority
from model.post_watchlist import PostWatchlist
from persistence.data.post_priority_data import get_priority_posts_to_crawl, delete_priority_post
from persistence.data.post_watchlist_data import get_posts_watchlist_to_crawl, \
    refresh_post_watchlist_expiration
from persistence.entity.post_control_entity import PostControlEntity


def create_post_control(url: str, service_identifier: str):
    PostControlEntity.create(url=url,
                             service_identifier=service_identifier,
                             last_visit=datetime.now().timestamp())


# Gets a post list to crawl performing a union of the default list with the priority list and the watchlist.
# For the default list, gets the posts to crawl according to a given proportion
# between posts to be visited for the first time and posts to be revisited. For posts to be revisited,
# the selection is given according to the following rule: the result of last_vist + delay, which gives the date from
# which it can be revisited, must be bellow now() the delay value can be modified depending on how many times it was
# visited without changes on info
def get_post_to_crawl(n_post: int, rate: float, service_identifier: str) -> list[str]:
    revisited_amount = int(n_post * rate)

    post_to_revisit_list: List[PostControlEntity] = PostControlEntity.select() \
        .where((PostControlEntity.service_identifier == service_identifier) &
               (PostControlEntity.is_new == False)) \
        .order_by(PostControlEntity.last_visit.asc()) \
        .limit(revisited_amount)

    new_post_to_visit_list: List[PostControlEntity] = PostControlEntity.select() \
        .where((PostControlEntity.service_identifier == service_identifier) &
               (PostControlEntity.is_new == True)) \
        .order_by(PostControlEntity.last_visit.asc()) \
        .limit(n_post - revisited_amount)

    post_urls_to_revisit: List[str] = [post_to_revisit.url for post_to_revisit in
                                       post_to_revisit_list]

    post_urls_to_visit: List[str] = [post_to_visit.url for post_to_visit in
                                     new_post_to_visit_list]

    post_priority_list: List[PostPriority] = get_priority_posts_to_crawl(n_post, service_identifier)
    post_watchlist_list: List[PostWatchlist] = get_posts_watchlist_to_crawl(n_post, service_identifier)

    post_priority_url_list: List[str] = [post_priority.url for post_priority in
                                         post_priority_list]

    post_watchlist_url_list: List[str] = [post.url for post in
                                          post_watchlist_list]

    work_list = post_urls_to_revisit + post_urls_to_visit + post_priority_url_list + \
                post_watchlist_url_list

    distinct_post_urls = set(work_list)

    # Remove items from priority list after returned to a service
    for post_priority_url in post_priority_url_list:
        delete_priority_post(post_priority_url, service_identifier)

    # Refresh the expiration dates of the selected posts to be revisited.
    for post_watchlist_url in post_watchlist_url_list:
        refresh_post_watchlist_expiration(post_watchlist_url, service_identifier)

    return list(distinct_post_urls)
