from datetime import datetime
from typing import List

from model.profile_priority import ProfilePriority
from model.profile_watchlist import ProfileWatchlist
from persistence.data.profile_priority_data import get_priority_profiles_to_crawl, delete_priority_profile
from persistence.data.profile_watchlist_data import get_profiles_watchlist_to_crawl, \
    refresh_profile_watchlist_expiration
from persistence.entity.profile_control_entity import ProfileControlEntity


def create_profile_control(username: str, service_identifier: str):
    ProfileControlEntity.create(username=username,
                                service_identifier=service_identifier,
                                last_visit=datetime.now().timestamp())


def refresh_profile_control_last_visit(username: str, service_identifier: str):
    query = ProfileControlEntity \
        .update({ProfileControlEntity.last_visit: datetime.now().timestamp()}) \
        .where((ProfileControlEntity.username == username) &
               (ProfileControlEntity.service_identifier == service_identifier))

    n_updated = query.execute()

    if n_updated == 0:
        create_profile_control(username, service_identifier)


# Gets a profile list to crawl performing a union of the default list with the priority list and the watchlist.
# For the default list, gets the profiles to crawl according to a given proportion
# between profiles to be visited for the first time and profiles to be revisited. For profiles to be revisited,
# the selection is given according to the following rule: the result of last_vist + delay, which gives the date from
# which it can be revisited, must be bellow now() the delay value can be modified depending on how many times it was
# visited without changes on info
def get_profile_to_crawl(n_profile: int, rate: float, service_identifier: str) -> list[str]:
    revisited_amount = int(n_profile * rate)

    profile_to_revisit_list: List[ProfileControlEntity] = ProfileControlEntity.select() \
        .where((ProfileControlEntity.service_identifier == service_identifier) &
               (ProfileControlEntity.is_new == False)) \
        .order_by(ProfileControlEntity.last_visit.asc()) \
        .limit(revisited_amount)

    new_profile_to_visit_list: List[ProfileControlEntity] = ProfileControlEntity.select() \
        .where((ProfileControlEntity.service_identifier == service_identifier) &
               (ProfileControlEntity.is_new == True)) \
        .order_by(ProfileControlEntity.last_visit.asc()) \
        .limit(n_profile - revisited_amount)

    usernames_to_revisit: List[str] = [profile_to_revisit.username for profile_to_revisit in
                                       profile_to_revisit_list]

    usernames_to_visit: List[str] = [profile_to_visit.username for profile_to_visit in
                                     new_profile_to_visit_list]

    profile_priority_list: List[ProfilePriority] = get_priority_profiles_to_crawl(n_profile, service_identifier)
    profile_watchlist_list: List[ProfileWatchlist] = get_profiles_watchlist_to_crawl(n_profile, service_identifier)

    profile_priority_username_list: List[str] = [profile_priority.username for profile_priority in
                                                 profile_priority_list]

    profile_watchlist_username_list: List[str] = [profile.username for profile in
                                                  profile_watchlist_list]

    work_list = usernames_to_revisit + usernames_to_visit + profile_priority_username_list + \
                profile_watchlist_username_list

    distinct_usernames = set(work_list)

    # Remove items from priority list after returned to a service
    for profile_priority_username in profile_priority_username_list:
        delete_priority_profile(profile_priority_username, service_identifier)

    # Refresh the expiration dates of the selected profiles to be revisited.
    for profile_watchlist_username in profile_watchlist_username_list:
        refresh_profile_watchlist_expiration(profile_watchlist_username, service_identifier)

    return list(distinct_usernames)
