from datetime import datetime
from typing import List

from model.post_watchlist import PostWatchlist
from persistence.entity.post_watchlist_entity import PostWatchListEntity


def add_post_watchlist(post: PostWatchlist):
    PostWatchListEntity.create(url=post.url,
                               service_identifier=post.service_identifier,
                               expiration_at=post.expiration_at,
                               created_at=int(datetime.now().timestamp()))


def get_posts_watchlist_to_crawl(n_posts: int, service_identifier: str) -> List[PostWatchlist]:
    post_watchlist_entity_list = PostWatchListEntity \
        .select() \
        .where(PostWatchListEntity.expiration_at <= int(datetime.now().timestamp()),
               PostWatchListEntity.service_identifier == service_identifier) \
        .order_by(PostWatchListEntity.expiration_at.asc()) \
        .limit(n_posts)

    return [from_entity(post_watchlist_entity) for post_watchlist_entity in post_watchlist_entity_list]


def refresh_post_watchlist_expiration(url: str, service_identifier: str):
    PostWatchListEntity \
        .update({PostWatchListEntity.expiration_at: PostWatchListEntity.expiration_at + (60 * 60 * 24 * 7)}) \
        .where(PostWatchListEntity.url == url,
               PostWatchListEntity.service_identifier == service_identifier) \
        .execute()


def from_entity(post_watchlist_entity: PostWatchListEntity) -> PostWatchlist:
    return PostWatchlist(id_=post_watchlist_entity.id,
                         url=post_watchlist_entity.url,
                         service_identifier=post_watchlist_entity.service_identifier,
                         created_at=post_watchlist_entity.created_at)
