from datetime import datetime
from typing import List

from model.profile_priority import ProfilePriority
from persistence.entity.profile_priority_entity import ProfilePriorityEntity


def add_priority_profile(profile: ProfilePriority):
    ProfilePriorityEntity.create(username=profile.username,
                                 service_identifier=profile.service_identifier,
                                 created_at=int(datetime.now().timestamp()))


def get_priority_profiles_to_crawl(n_profiles: int, service_identifier: str) -> List[ProfilePriority]:
    profile_priority_entity_list = ProfilePriorityEntity \
        .select() \
        .where(ProfilePriorityEntity.service_identifier == service_identifier) \
        .order_by(ProfilePriorityEntity.created_at.asc()) \
        .limit(n_profiles)

    return [from_entity(profile_priority_entity) for profile_priority_entity in profile_priority_entity_list]


def delete_priority_profile(username: str, service_identifier: str):
    ProfilePriorityEntity \
        .delete() \
        .where(ProfilePriorityEntity.username == username,
               ProfilePriorityEntity.service_identifier == service_identifier) \
        .execute()


def from_entity(profile_priority_entity: ProfilePriorityEntity) -> ProfilePriority:
    return ProfilePriority(id_=profile_priority_entity.id,
                           username=profile_priority_entity.username,
                           service_identifier=profile_priority_entity.service_identifier,
                           created_at=profile_priority_entity.created_at)
