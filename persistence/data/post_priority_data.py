from datetime import datetime
from typing import List

from model.post_priority import PostPriority
from persistence.entity.post_priority_entity import PostPriorityEntity


def add_priority_post(post: PostPriority):
    PostPriorityEntity.create(url=post.url,
                              service_identifier=post.service_identifier,
                              created_at=int(datetime.now().timestamp()))


def get_priority_posts_to_crawl(n_posts: int, service_identifier: str) -> List[PostPriority]:
    post_priority_entity_list = PostPriorityEntity \
        .select() \
        .where(PostPriorityEntity.service_identifier == service_identifier) \
        .order_by(PostPriorityEntity.created_at.asc()) \
        .limit(n_posts)

    return [from_entity(post_priority_entity) for post_priority_entity in post_priority_entity_list]


def delete_priority_post(url: str, service_identifier: str):
    PostPriorityEntity \
        .delete() \
        .where(PostPriorityEntity.url == url,
               PostPriorityEntity.service_identifier == service_identifier) \
        .execute()


def from_entity(post_priority_entity: PostPriorityEntity) -> PostPriority:
    return PostPriority(id_=post_priority_entity.id,
                        url=post_priority_entity.url,
                        service_identifier=post_priority_entity.service_identifier,
                        created_at=post_priority_entity.created_at)
