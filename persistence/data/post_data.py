from datetime import datetime
from typing import List, Optional

from peewee import fn, DoesNotExist

import persistence.data.comment_data as comment_data
import persistence.data.like_on_post_data as like_on_post_data
import persistence.data.profile_data as profile_data
from model.post import Post
from persistence.entity.post_entity import PostEntity


def save_post(post: Post):
    post_on_db: PostEntity = PostEntity.get_or_create(url=post.url)[0]

    now = int(datetime.now().timestamp())
    if post_on_db.created_at is None:
        post.created_at = now
    else:
        post.created_at = post_on_db.created_at

    post.last_visit = now

    post.id_ = post_on_db.id
    post_entity: PostEntity = to_entity(post)

    post_entity.save()

    if post.comments is not None:
        for comment in post.comments:
            comment_data.create_or_update_comment(comment, post)

    if post.likers is not None:
        for profile in post.likers:
            like_on_post_data.create_or_update_like_on_post(post, profile)


def get_or_create_post(url: str) -> Post:
    return from_entity(PostEntity.get_or_create(url=url)[0])


def get_post(url: str) -> Optional[Post]:
    try:
        return from_entity(PostEntity.get(PostEntity.url == url))
    except DoesNotExist:
        return None


# gets posts to crawl acording to a given proportion between posts to be visited for the first time and posts to be revisited.
# For posts to be revisited, the selection is given according to the following rule:
# the result of last_vist + delay, which gives the date from which it can be revisited, must be bellow now()
# the delay value can be modified depending on how many times it was visited without changes on info
def get_posts_to_crawl(n_posts: int, rate: float) -> List[Post]:
    revisted_amount = int(n_posts * rate)
    post_entity_list_to_be_revisited: List[PostEntity] = PostEntity.select() \
        .where((PostEntity.last_visit + fn.COALESCE(PostEntity.delay, 0)) <= int(datetime.now().timestamp())) \
        .order_by((PostEntity.last_visit + fn.COALESCE(PostEntity.delay, 0)).asc()) \
        .limit(revisted_amount)
    # TODO: tratar proporção

    post_entity_list_to_be_visited: List[PostEntity] = PostEntity.select() \
        .where(PostEntity.last_visit.is_null(True)) \
        .order_by(PostEntity.created_at.asc()) \
        .limit(n_posts - revisted_amount)
    # TODO: tratar proporção

    to_visit = post_entity_list_to_be_visited + post_entity_list_to_be_revisited

    return [from_entity(post) for post in to_visit]


def str_to_list(input: str) -> List[str]:
    return input and (input.replace('[', '').replace('\'', '').replace(' ', '').replace(']', '').split(
        ','))


def from_entity(post_entity: PostEntity) -> Post:
    return Post(id_=post_entity.id,
                profile=profile_data.from_entity(post_entity.profile) if post_entity.profile is not None else None,
                url=post_entity.url,
                url_imgs=str_to_list(post_entity.url_imgs),
                post_date=post_entity.post_date,
                caption=post_entity.caption,
                last_visit=post_entity.last_visit,
                created_at=post_entity.created_at,
                deleted=post_entity.deleted,
                likes=post_entity.likes)


def to_entity(post: Post) -> PostEntity:
    return PostEntity(id=post.id_,
                      profile=profile_data.to_entity(post.profile),
                      url=post.url,
                      url_imgs=post.url_imgs,
                      post_date=post.post_date,
                      caption=post.caption,
                      last_visit=post.last_visit,
                      created_at=post.created_at,
                      deleted=post.deleted,
                      likes=post.likes)
