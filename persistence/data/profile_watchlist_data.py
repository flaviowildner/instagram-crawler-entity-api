from datetime import datetime
from typing import List

from model.profile_watchlist import ProfileWatchlist
from persistence.entity.profile_watchlist_entity import ProfileWatchListEntity


def add_profile_watchlist(profile: ProfileWatchlist):
    ProfileWatchListEntity.create(username=profile.username,
                                  service_identifier=profile.service_identifier,
                                  expiration_at=profile.expiration_at,
                                  created_at=int(datetime.now().timestamp()))


def get_profiles_watchlist_to_crawl(n_profiles: int, service_identifier: str) -> List[ProfileWatchlist]:
    profile_watchlist_entity_list = ProfileWatchListEntity \
        .select() \
        .where(ProfileWatchListEntity.expiration_at <= int(datetime.now().timestamp()),
               ProfileWatchListEntity.service_identifier == service_identifier) \
        .order_by(ProfileWatchListEntity.expiration_at.asc()) \
        .limit(n_profiles)

    return [from_entity(profile_watchlist_entity) for profile_watchlist_entity in profile_watchlist_entity_list]


def refresh_profile_watchlist_expiration(username: str, service_identifier: str):
    ProfileWatchListEntity \
        .update({ProfileWatchListEntity.expiration_at: ProfileWatchListEntity.expiration_at + (60 * 60 * 24 * 7)}) \
        .where(ProfileWatchListEntity.username == username,
               ProfileWatchListEntity.service_identifier == service_identifier) \
        .execute()


def from_entity(profile_watchlist_entity: ProfileWatchListEntity) -> ProfileWatchlist:
    return ProfileWatchlist(id_=profile_watchlist_entity.id,
                            username=profile_watchlist_entity.username,
                            service_identifier=profile_watchlist_entity.service_identifier,
                            created_at=profile_watchlist_entity.created_at)
