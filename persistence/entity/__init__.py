from persistence.entity import *
from persistence.entity.base_model import psql_db
from persistence.entity.comment_entity import CommentEntity
from persistence.entity.following_entity import FollowingEntity
from persistence.entity.like_on_comment_entity import LikeOnCommentEntity
from persistence.entity.like_on_post_entity import LikeOnPostEntity
from persistence.entity.post_control_entity import PostControlEntity
from persistence.entity.post_entity import PostEntity
from persistence.entity.post_media_entity import PostMediaEntity
from persistence.entity.post_priority_entity import PostPriorityEntity
from persistence.entity.post_watchlist_entity import PostWatchListEntity
from persistence.entity.profile_control_entity import ProfileControlEntity
from persistence.entity.profile_entity import ProfileEntity

from peewee import PostgresqlDatabase
from constants.consts import DB
from persistence.entity.profile_priority_entity import ProfilePriorityEntity
from persistence.entity.profile_watchlist_entity import ProfileWatchListEntity


def reset_db():
    psql_db = PostgresqlDatabase(DB['database'], user=DB['user'], password=DB['password'], host=DB['host'])

    psql_db.drop_tables(
        [ProfileEntity, PostEntity, CommentEntity, LikeOnPostEntity, LikeOnCommentEntity, FollowingEntity],
        cascade=True)

    ProfileEntity.create_table()
    PostEntity.create_table()
    CommentEntity.create_table()
    LikeOnPostEntity.create_table()
    LikeOnCommentEntity.create_table()
    FollowingEntity.create_table()
    PostMediaEntity.create_table()
    ProfilePriorityEntity.create_table()
    PostPriorityEntity.create_table()
    ProfileWatchListEntity.create_table()
    PostWatchListEntity.create_table()
    ProfileControlEntity.create_table()
    PostControlEntity.create_table()

# Uncomment to re-create tables
# reset_db()

# print('test_MIGR')
# psql_db.evolve()
