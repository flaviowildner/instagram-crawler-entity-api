from peewee import PostgresqlDatabase, Model

from constants.consts import DB

psql_db = PostgresqlDatabase(DB['database'], user=DB['user'], password=DB['password'], host=DB['host'], autorollback=True)


class BaseModel(Model):
    class Meta:
        database = psql_db