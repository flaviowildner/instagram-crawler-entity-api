from peewee import TextField, ForeignKeyField, BlobField, CompositeKey, BigIntegerField

from .base_model import BaseModel
from .post_entity import PostEntity


# TODO create indexes
class PostMediaEntity(BaseModel):
    post = ForeignKeyField(PostEntity, null=True)
    media_url = TextField(null=True)
    extension = TextField(null=True)
    data = BlobField(null=True)
    post_media_date = BigIntegerField(null=True)

    class Meta:
        primary_key = CompositeKey('post', 'media_url')
        table_name = 'post_media'
