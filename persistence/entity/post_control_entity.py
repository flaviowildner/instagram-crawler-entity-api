from peewee import TextField, BigIntegerField, BooleanField, CompositeKey

from persistence.entity.base_model import BaseModel


class PostControlEntity(BaseModel):
    url = TextField(null=False)
    service_identifier = TextField(null=False)
    is_new = BooleanField(default=True)
    last_visit = BigIntegerField(null=True)

    class Meta:
        primary_key = CompositeKey('url', 'service_identifier')
        table_name = 'post_control'
