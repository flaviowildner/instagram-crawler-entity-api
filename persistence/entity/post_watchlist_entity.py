from peewee import AutoField, TextField, BigIntegerField

from persistence.entity.base_model import BaseModel


# TODO create indexes
class PostWatchListEntity(BaseModel):
    id = AutoField(null=False)
    url = TextField(null=False)
    service_identifier = TextField(null=False)
    expiration_at = BigIntegerField(null=True)
    created_at = BigIntegerField(null=True)

    class Meta:
        table_name = 'post_watchlist'
