from peewee import TextField, BigIntegerField, BooleanField, CompositeKey

from persistence.entity.base_model import BaseModel


class ProfileControlEntity(BaseModel):
    username = TextField(null=False)
    service_identifier = TextField(null=False)
    is_new = BooleanField(default=True)
    last_visit = BigIntegerField(null=True)

    class Meta:
        primary_key = CompositeKey('username', 'service_identifier')
        table_name = 'profile_control'
