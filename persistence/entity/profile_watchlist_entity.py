from peewee import AutoField, TextField, BigIntegerField

from persistence.entity.base_model import BaseModel


# TODO create indexes
class ProfileWatchListEntity(BaseModel):
    id = AutoField(null=False)
    username = TextField(null=False)
    service_identifier = TextField(null=False)
    expiration_at = BigIntegerField(null=True)
    created_at = BigIntegerField(null=True)

    class Meta:
        table_name = 'profile_watchlist'
