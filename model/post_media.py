from typing import Optional

from model.post import Post


class PostMedia:
    post: Optional[Post]
    media_url: Optional[str]
    extension: Optional[str]
    data: Optional[bytes]
    post_media_date: Optional[int]

    def __init__(self, post: Optional[Post] = None,
                 media_url: Optional[str] = None,
                 extension: Optional[str] = None,
                 data: Optional[bytes] = None,
                 post_media_date: Optional[int] = None) -> None:
        self.post = post
        self.media_url = media_url
        self.extension = extension
        self.data = data
        self.post_media_date = post_media_date
