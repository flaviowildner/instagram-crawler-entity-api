from typing import Optional


class PostWatchlist:
    id_: Optional[int]
    url: Optional[str]
    service_identifier: Optional[str]
    expiration_at: Optional[int]
    created_at: Optional[int]

    def __init__(self, id_: Optional[int] = None,
                 url: Optional[str] = None,
                 service_identifier: Optional[str] = None,
                 expiration_at: Optional[int] = None,
                 created_at: Optional[int] = None) -> None:
        self.id_ = id_
        self.url = url
        self.service_identifier = service_identifier
        self.expiration_at = expiration_at
        self.created_at = created_at
