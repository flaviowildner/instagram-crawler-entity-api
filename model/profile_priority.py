from typing import Optional


class ProfilePriority:
    id_: Optional[int]
    username: Optional[str]
    service_identifier: Optional[str]
    created_at: Optional[int]

    def __init__(self, id_: Optional[int] = None,
                 username: Optional[str] = None,
                 service_identifier: Optional[str] = None,
                 created_at: Optional[int] = None) -> None:
        self.id_ = id_
        self.username = username
        self.service_identifier = service_identifier
        self.created_at = created_at
