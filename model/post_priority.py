from typing import Optional


class PostPriority:
    id_: Optional[int]
    url: Optional[str]
    service_identifier: Optional[str]
    created_at: Optional[int]

    def __init__(self, id_: Optional[int] = None,
                 url: Optional[str] = None,
                 service_identifier: Optional[str] = None,
                 created_at: Optional[int] = None) -> None:
        self.id_ = id_
        self.url = url
        self.service_identifier = service_identifier
        self.created_at = created_at
