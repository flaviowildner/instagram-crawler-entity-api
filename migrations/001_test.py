"""Peewee migrations."""
from persistence.entity import ProfilePriorityEntity, PostPriorityEntity, ProfileWatchListEntity, PostWatchListEntity, \
    ProfileControlEntity, PostControlEntity
from persistence.entity.comment_entity import CommentEntity
from persistence.entity.following_entity import FollowingEntity
from persistence.entity.like_on_comment_entity import LikeOnCommentEntity
from persistence.entity.like_on_post_entity import LikeOnPostEntity
from persistence.entity.post_entity import PostEntity
from persistence.entity.post_media_entity import PostMediaEntity
from persistence.entity.profile_entity import ProfileEntity


def migrate(migrator, database, **kwargs):
    migrator.create_table(ProfileEntity)
    migrator.create_table(PostEntity)
    migrator.create_table(CommentEntity)
    migrator.create_table(LikeOnPostEntity)
    migrator.create_table(LikeOnCommentEntity)
    migrator.create_table(FollowingEntity)
    migrator.create_table(PostMediaEntity)
    migrator.create_table(ProfilePriorityEntity)
    migrator.create_table(PostPriorityEntity)
    migrator.create_table(ProfileWatchListEntity)
    migrator.create_table(PostWatchListEntity)
    migrator.create_table(ProfileControlEntity)
    migrator.create_table(PostControlEntity)
