# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask import Flask

from logger import start_logger
from resources.post_control_resource import post_control_blueprint
from resources.profile_control_resource import profile_control_blueprint
from resources.priority_resource import priority_profile_blueprint
from resources.post_media_resource import post_media_blueprint
from resources.post_resource import post_blueprint
from resources.profile_resource import profile_blueprint
from run_migration import run_migrations

start_logger()

run_migrations()

app: Flask = Flask(__name__)

app.register_blueprint(profile_blueprint)
app.register_blueprint(post_blueprint)
app.register_blueprint(post_media_blueprint)
app.register_blueprint(priority_profile_blueprint)
app.register_blueprint(profile_control_blueprint)
app.register_blueprint(post_control_blueprint)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
