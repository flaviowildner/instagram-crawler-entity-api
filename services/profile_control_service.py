from typing import List

from peewee import IntegrityError

from constants.consts import SERVICE_IDENTIFIERS_FOR_PROFILES
from persistence.data import profile_control_data
from persistence.data.profile_control_data import get_profile_to_crawl


def get_n_to_crawl(n_profiles: int, rate: float, service_identifier: str) -> List[str]:
    return get_profile_to_crawl(n_profiles, rate, service_identifier)


def create_profile_control(username: str):
    for service_identifier in SERVICE_IDENTIFIERS_FOR_PROFILES:
        try:
            profile_control_data.create_profile_control(username, service_identifier)
        except IntegrityError:
            pass


def refresh_profile_control_last_visit(username: str, service_identifier: str):
    profile_control_data.refresh_profile_control_last_visit(username, service_identifier)
