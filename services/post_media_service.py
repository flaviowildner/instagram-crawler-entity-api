from peewee import DatabaseError

from model.post_media import PostMedia
from persistence.data import post_media_data


def save(post_media: PostMedia):
    try:
        return post_media_data.save(post_media)
    except DatabaseError as e:
        raise RuntimeError(e)
