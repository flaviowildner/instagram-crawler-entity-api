from typing import List

from peewee import DatabaseError

from model.profile import Profile
from persistence.data.profile_control_data import get_profile_to_crawl
from persistence.data.profile_data import create_or_update_profile, get_or_create_profile
from services import profile_control_service
from utils.util import merge_dict


# class to define the ratio used on get profiles_to_crawl
# rate = % of revisited profiles brought by the function
class RateConfig:

    def __init__(self, rate: float = 0.5):
        self.rate = rate


def get_n_to_crawl(n_profiles: int, rate: float, service_identifier: str) -> List[str]:
    return get_profile_to_crawl(n_profiles, rate, service_identifier)


def create_or_update(profile: Profile, service_identifier: str) -> Profile:
    try:
        existing_profile = get_or_create(profile.username)

        existing_profile_dict = existing_profile.__dict__
        profile_dict = profile.__dict__

        merged_profile_dict = merge_dict(profile_dict, existing_profile_dict)
        updated_profile = Profile(**merged_profile_dict)

        create_or_update_profile(updated_profile)

        profile_control_service.refresh_profile_control_last_visit(updated_profile.username, service_identifier)
    except DatabaseError as e:
        raise RuntimeError(e)

    return updated_profile


def get_or_create(username: str):
    try:
        return get_or_create_profile(username)
    except DatabaseError as e:
        raise RuntimeError(e)
