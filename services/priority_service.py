from model.profile_priority import ProfilePriority
from persistence.data import profile_priority_data


def add_priority_profile(username: str, service_identifier: str):
    profile_priority = ProfilePriority(username=username, service_identifier=service_identifier)

    profile_priority_data.add_priority_profile(profile_priority)
