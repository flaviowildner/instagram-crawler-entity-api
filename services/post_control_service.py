from typing import List

from constants.consts import SERVICE_IDENTIFIERS_FOR_POSTS_URLS
from persistence.data import post_control_data
from persistence.data.post_control_data import get_post_to_crawl


def get_n_to_crawl(n_posts: int, rate: float, service_identifier: str) -> List[str]:
    return get_post_to_crawl(n_posts, rate, service_identifier)


def create_post_control(url: str) -> None:
    for service_identifier in SERVICE_IDENTIFIERS_FOR_POSTS_URLS:
        post_control_data.create_post_control(url, service_identifier)
