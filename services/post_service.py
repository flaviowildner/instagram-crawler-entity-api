from typing import List

from peewee import DatabaseError

from model.post import Post
from persistence.data.post_data import get_posts_to_crawl, get_or_create_post, save_post, get_post
from utils.util import merge_dict

#class to define the ratio used on get post_to_crawl
#rate = % of revisited posts brought by the function
class RateConfig:

    def __init__(self, rate: float = 0.5):
        self.rate = rate


def get_n_to_crawl(n_posts: int, rate: float) -> List[Post]:
    return get_posts_to_crawl(n_posts, rate)


def save(post: Post):
    try:
        existing_post = get_or_create_post(post.url)

        existing_post_dict = existing_post.__dict__
        post_dict = post.__dict__

        merged_profile_dict = merge_dict(post_dict, existing_post_dict)
        updated_post = Post(**merged_profile_dict)

        save_post(updated_post)
    except DatabaseError as e:
        raise RuntimeError(e)

    return updated_post


def get(url: str):
    try:
        return get_post(url)
    except DatabaseError as e:
        raise RuntimeError(e)
